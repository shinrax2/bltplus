if not BLTplus then return end

local hijacked_mod = nil


Hooker:PreHook('BLTViewModGui._setup_mod_info', function(mod)
	hijacked_mod = mod
end)


Hooker:Hook('BLTViewModGui._setup_mod_info', function(mod)
	hijacked_mod = nil
end)


Hooker:PreHook('BLTViewModGui.make_title', function(...)
	local args = {...}

	local cat = hijacked_mod:GetCategory()
	if hijacked_mod and cat then
		args[1] = cat..' => '..args[1]
	end
	
	Hooker:Args(nil, args)
end)


Hooker:Hook('BLTViewModGui:clbk_toggle_updates_state', function(self)
	BLTplus:ToggleUpdates(self._mod)
end)