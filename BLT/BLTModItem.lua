if not BLTplus then return end

-- How many mods to show in a single row of the mod manager
BLTModItem.layout = BLTModItem.layout or {}
BLTModItem.layout.x = (BLTplus.menu.mods_menu.images_row.value or 4) + 1

-- Sort mods with images first
table.sort(BLT.Mods.mods, function(a,b)
								if a:HasModImage() == b:HasModImage() then
									return a:GetName() < b:GetName()
								end
								if a:HasModImage() then
									return true
								else
									return false
								end
							end)


-- This below assignes indexes to mods in the mods menu
local taken = {}
taken[1] = true
local last_img_inx = 1
local first_small_done = false
Hooker:PreHook('BLTModItem.init', function(...)
	local args = {...}
	local index, mod, show_icon = args[2], args[3], args[4]
	
	if not BLTplus.menu.mods_menu.change_menu then
		if not BLTModsGui.show_mod_icons and index == BLTModItem.layout.x + 1 then
			local tweaks = { 0, 2, 1, 1, -1, -2, -3}
			index = index + (tweaks[BLTModItem.layout.x] or 0)
		end
		
		args[2] = index
		Hooker:Args(nil, args)
		return
	end

	if BLTModsGui.show_mod_icons then
		if index == 2 then
			last_img_inx = 1
			taken = {}
			taken[1] = true
			first_small_done = false
		end
			
		if mod:HasModImage() and mod:GetModImage() then
			while taken[index] do
				index = index + 1
			end
			
			taken[index] = true
			last_img_inx = last_img_inx + 1
		else
			show_icon = false
			
			while taken[index]
				or index < last_img_inx * 2
				or index < math.ceil(last_img_inx / BLTModItem.layout.x) * 2 * BLTModItem.layout.x
			do
				taken[index] = true
				index = index + 1
			end
			
			if not first_small_done then
				while index % BLTModItem.layout.x ~= 1 do
					taken[index] = true
					index = index + 1
				end
				first_small_done = true
			end
			taken[index] = true
		end
	elseif index == BLTModItem.layout.x + 1 then
		local tweaks = { 0, 2, 1, 1, -1, -2, -3}
		index = index + (tweaks[BLTModItem.layout.x] or 0)
	end
	
	args[2] = index
	args[4] = show_icon
	Hooker:Args(nil, args)
end)