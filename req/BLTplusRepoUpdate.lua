BLTplusRepoUpdate = blt_class(BLTUpdate)

function BLTplusRepoUpdate:init(parent_mod, data)

	self.repo_id = data['host'].user .. '@' .. data['host'].project
	
	data.identifier = data.identifier or self.repo_id

	-- This makes sure BLT won't crash the game when treating this as a normal update
	BLTplusRepoUpdate.super.init(self, parent_mod, data)
	
	self.new_data = {}
	
	self.BLTplusRepoUpdate = true
end


function BLTplusRepoUpdate.CheckAndCreateDir(dir)
	local path = ''
	for v in dir:gmatch('[^/\\]+[/\\]', '') do
		path = path .. v
		if not file.DirectoryExists(path) then
			log("[BLT+] Creating directory "..path)
			file.CreateDirectory(path)
		end
	end
end


function BLTplusRepoUpdate:CheckForUpdates(clbk)
	self.clbk_got_update = clbk
	
	self.mod_id = self:GetParentMod():GetPureId()
	self.hash = BLTplus:GetRepoHash(self.mod_id, self.repo_id)
	
	if not self.hash and self.host.this_hash and self.host.this_hash ~= '' then -- Use hash from mod.txt if present
		BLTplus:SetRepoHash(self.mod_id, self.repo_id, self.host.this_hash)
		self.hash = self.host.this_hash
	end
	
	if type(self.hash) ~= 'number' then
		BLTplusC.repos_api.GetLastCommit(self.host, callback(self, self, 'clbk_got_update_data'))
	else
		BLTplus:SetRepoHash(self.mod_id, self.repo_id, self.hash > 0 and self.hash - 1 or nil)
	end
end


function BLTplusRepoUpdate:clbk_got_update_data(success, data1, data2)

	self._requesting_updates = false
	
	if not success then
		log('[Updates+] Failed to fetch repo data for \''..self.mod_id..'\': '..data1)
		
		BLTplus:FailedUpdate(self.mod_id)

		if BLTplus:GetFailedUpdate(self.mod_id) > 5 then
			BLTplus:FailedUpdate(self.mod_id, 0)
			BLTplus:SetRepoHash(self.mod_id, self.repo_id, 5)
			BLTplus:Save()
		end
		
		return
	end
		
	log('[Updates+] Received data from \''..(self.host.platform or 'nil')..'\' for the mod \''..self.mod_id..'\'')
		
	local newhash = data1
	local message = data2
		
	if self.hash then
		if self.hash ~= newhash then
	   
			BLTplusC.repos_api.GetFilesToUpdate(self.host, self.hash, function(success, data1, data2)
				if not success then
					log('[Updates+] Failed to fetch the list of updated files: '..data1)
					return
				end
				
				self.filenames = data1
				self.removed_files = data2
				self.raw_url =  BLTplusC.repos_api.GetRawFileUrl(self.host)
				self.hash = newhash
				
				if self.clbk_got_update then
					self.clbk_got_update(self, true)
				end
			end)
			
		end
	else
		BLTplus:SetRepoHash(self.mod_id, self.repo_id, newhash)
	end
end


function BLTplusRepoUpdate:ViewPatchNotes()
	local url = BLTplusC.repos_api.GetCommitsUrl(self.host)
	if url then
		if Steam:overlay_enabled() then
			Steam:overlay_activate( "url", url )
		else
			os.execute( "cmd /c start " .. url )
		end
	end
end


function BLTplusRepoUpdate:Update(inx, progress_clbk, fin_clbk)
	
	if not inx then
		log('[Updates+] Downloading '..self.id)
		inx = 1
	else
		inx = inx + 1
	end
	
	if self.filenames[inx] then
		if progress_clbk then
			progress_clbk(self.id, inx, #self.filenames)
		end
		dohttpreq(BLTplusC.repos_api.FormatRawFileUrl(self.raw_url, self.filenames[inx]), function(body, http_id)
			if body:is_nil_or_empty() then
				fin_clbk(self.id, false, 'Empty data received for file '..self.filenames[inx])
				self.new_data = {}
				return
			end
			
			-- Got a 404 or something?
			if body:match('^[^\'"\n]*(html|HTML)') then
				-- Sure it's not a lua file?
				if not body:match('[^\n%-]+(local|end)') then
					fin_clbk(self.id, false, 'Platform returned an error for file '..self.filenames[inx])
					log(body)
					self.new_data = {}
					return
				end
			end
			
			self.new_data[self.filenames[inx]] = body
			
			self:Update(inx, progress_clbk, fin_clbk)
		end)
	else
		-- Check that we have everything
		for k,v in ipairs(self.filenames) do
			if not self.new_data[v] then
				fin_clbk(self.id, false, 'No data received for file '..v)
				self.new_data = {}
				return
			end
		end
		
		log('[Updates+] Updating '..self:GetParentMod():GetName()..' from '..self.repo_id)
		for k,v in ipairs(self.filenames) do
			local path = self:GetParentMod():GetPath() .. v
			
			BLTplusRepoUpdate.CheckAndCreateDir(path)
			
			log('[Updates+] Updating '..path)
			
			local file = io.open(path, "wb+")
			if file then
				file:write(self.new_data[v])
				file:close()
			else
				fin_clbk(self.id, false, 'IO Error for: '..v)
				self.new_data = {}
				return
			end
		end
		
		for k,v in ipairs(self.removed_files) do
			local path = self:GetParentMod():GetPath() .. v
			log('[Updates+] Removing '..path)
			os.remove(path)
		end
		
		BLTplus:FailedUpdate(self.mod_id, 0)
		BLTplus:SetRepoHash(self.mod_id, self.repo_id, self.hash)
		BLTplus:Save()
		
		fin_clbk(self.id, true)
	end
end