BLTplusC = BLTplusC or {}

BLTplusC.loc = {
	no = 'No',
	ok = 'OK',

	two_mods = 'There are two mods with such folder name:',
	first_loaded = 'The first one has been loaded up.',
	
	older_blt_loaded = 'This mod is for an older version of BLT but it is loaded up.\n\nIs it working correctly?',
	older_blt_not_loaded = 'This mod is for an older version of BLT and is not loaded up.\n\nWould you like to enable it?',
	yes_upgrade = 'Yes, make it work with new BLT',
	
	check_compatibility = 'Check compatibility',
	mods_not_checked = 'These mods have not been checked for compatibility with BLT+:',
	check_now = 'Check now',
	check_later = 'Later',
	
	offline = 'Offline :(',
	updates_not_checked = 'Could not check for updates.',
	
	mod_fails_updates = 'The mod named \'%s\' has been failing to update recently.\n\nWould you like to disable updates for it?',
	yes_disable_updates = 'Yes, disable its updates',
	
	mod_updated_repo = 'This mod has been updated in its \'%s\' repository:', -- %s is github, for example
	no_message = '(No message available)',
	disable_forawhile = 'Disable this for some time',
	
	all_mods_work = 'These mods should be working correctly.',
	these_not_work = 'These mods might not work correctly from folder other than \'mods\':',
	saved_to_log = '(This has been saved to the log)',
	mods_check = 'Mods check', -- the title of the quickmenu with check result
	
	lang_available = 'This language is available for BLT+: %s',
	use_lang = 'Use %s', -- Use language
	stay_with_lang = 'Stay with %s', -- Stay with language
	
	not_found_update = 'Could not find this mod to add an update for it: %s',
	
	crashlog = 'Crash report',
	copy_crashlog = 'Save crash report to desktop'
}


BLTplusC.loc_menu = {

    base_plus_menu = 'BLT+ Settings',
    base_plus_menu_desc = 'BLT+ Settings',

    base_plus_menu_images_preload = 'Preload images',
    base_plus_menu_images_preload_desc = 'Load up mod images at game start',

    base_plus_menu_notifs_thru_updates = 'Notifications from updates',
    base_plus_menu_notifs_thru_updates_desc = 'If you want to get notifications from downloaded updates info',

    base_plus_menu_mods_menu = 'Mods menu',
    base_plus_menu_mods_menu_desc = 'BLT mod manager menu settings',

    base_plus_menu_mods_menu_change_menu = 'Change menu',
    base_plus_menu_mods_menu_change_menu_desc = 'Change the way mods are sorted',

    base_plus_menu_mods_menu_images_row = 'Mods in row',
    base_plus_menu_mods_menu_images_row_desc = 'How many mods to display in one row',
    base_plus_menu_mods_menu_images_row_two = '2',
    base_plus_menu_mods_menu_images_row_three = '3',
    base_plus_menu_mods_menu_images_row_four = '4',
    base_plus_menu_mods_menu_images_row_five = '5',
    base_plus_menu_mods_menu_images_row_six = '6',
    base_plus_menu_mods_menu_images_row_seven = '7',

    base_plus_menu_should_load = 'Manage categories',
    base_plus_menu_should_load_desc = 'Disable and enable mod categories',
	
    base_plus_menu_actions = 'BLT+ Notifications',
    base_plus_menu_actions_desc = 'See what actions are required',
	
    base_plus_menu_hide_notification = 'Show BLT+ icon',
    base_plus_menu_hide_notification_desc = 'When the notification icon should be displayed',
    base_plus_menu_hide_notification_show = 'Show',
    base_plus_menu_hide_notification_show_notifs = 'If notifications',
    base_plus_menu_hide_notification_hide = 'Hide',
	
    base_plus_menu_check_mods = 'Check compatibility (wait for it)',
    base_plus_menu_check_mods_desc = 'Check mods for being able to work from category folders',
	
    base_plus_menu_actions_bltp_check_mods = 'Check compatibility',
    base_plus_menu_actions_bltp_check_mods_desc = 'Check mods for being able to work from category folders'

}