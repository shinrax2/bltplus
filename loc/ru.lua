BLTplusC = BLTplusC or {}

BLTplusC.loc = {
	no = 'Нет',
	ok = 'ОК',

	two_mods = 'У двух модов одинаковое название папки:',
	first_loaded = 'Был загружен первый мод.',
	
	older_blt_loaded = 'Это мод для старой версии BLT, но он сейчас загружен.\n\nПравильно ли он работает?',
	older_blt_not_loaded = 'Это мод для старой версии BLT, и он не загружен.\n\nВы хотите его включить?',
	yes_upgrade = 'Да, пусть работает с новым BLT',
	
	check_compatibility = 'Проверить совместимость',
	mods_not_checked = 'Эти моды не проверялись на совместимость с BLT+:',
	check_now = 'Проверить сейчас',
	check_later = 'Позже',
	
	offline = 'Нет соединения :(',
	updates_not_checked = 'Не удалось проверить обновления.',
	
	mod_fails_updates = 'Мод \'%s\' не может обновиться в последнее время.\n\nОтключить для него проверку обновлений?',
	yes_disable_updates = 'Да, отключить обновления',
	
	mod_updated_repo = 'Этот мод был обновлен в его репозитории на \'%s\':', -- %s is github, for example
	no_message = '(Нет сообщения)',
	disable_forawhile = 'Откл. это сообщение на время',
	
	all_mods_work = 'Эти моды должны работать нормально.',
	these_not_work = 'Эти моды, возможно, не смогут правильно работать, если они не в папке \'mods\':',
	saved_to_log = '(Это сохранено в лог)',
	mods_check = 'Проверка модов', -- the title of the quickmenu with check result
	
	lang_available = 'Этот язык доступен для BLT+: %s',
	use_lang = 'Использовать %s', -- Use language
	stay_with_lang = 'Оставить %s', -- Stay with language
	
	not_found_update = 'Не удалось найти этот мод, чтобы добавить обновления для него: %s',
	
	crashlog = 'Отчет о сбое',
	copy_crashlog = 'Сохранить на рабочий стол'
}


BLTplusC.loc_menu = {

    base_plus_menu = 'BLT+ Настройки',
    base_plus_menu_desc = 'BLT+ Настройки',

    base_plus_menu_images_preload = 'Предзагрузка изображений',
    base_plus_menu_images_preload_desc = 'Загружать изображения модов при запуске игры',

    base_plus_menu_notifs_thru_updates = 'Уведомления из обновлений',
    base_plus_menu_notifs_thru_updates_desc = 'Хотите ли вы получать уведомления из информации обновлений',

    base_plus_menu_mods_menu = 'Меню модов',
    base_plus_menu_mods_menu_desc = 'Настройки меню менеджера модов BLT',

    base_plus_menu_mods_menu_change_menu = 'Изменять меню',
    base_plus_menu_mods_menu_change_menu_desc = 'Изменять способ сортировки модов',

    base_plus_menu_mods_menu_images_row = 'Модов в ряду',
    base_plus_menu_mods_menu_images_row_desc = 'Сколько модов отображать в одном ряду',
    base_plus_menu_mods_menu_images_row_two = '2',
    base_plus_menu_mods_menu_images_row_three = '3',
    base_plus_menu_mods_menu_images_row_four = '4',
    base_plus_menu_mods_menu_images_row_five = '5',
    base_plus_menu_mods_menu_images_row_six = '6',
    base_plus_menu_mods_menu_images_row_seven = '7',

    base_plus_menu_should_load = 'Управление категориями',
    base_plus_menu_should_load_desc = 'Выключайте и включайте категории модов',
	
    base_plus_menu_actions = 'BLT+ Уведомления',
    base_plus_menu_actions_desc = 'Посмотрите, что требует внимания',
	
    base_plus_menu_hide_notification = 'Иконка BLT+',
    base_plus_menu_hide_notification_desc = 'Когда отображать иконку уведомлений',
    base_plus_menu_hide_notification_show = 'Показывать',
    base_plus_menu_hide_notification_show_notifs = 'Уведомления',
    base_plus_menu_hide_notification_hide = 'Скрыть',
	
    base_plus_menu_check_mods = 'Проверить совместимость (подождите)',
    base_plus_menu_check_mods_desc = 'Проверить, могут ли моды работать из категорий',
	
    base_plus_menu_actions_bltp_check_mods = 'Проверить совместимость',
    base_plus_menu_actions_bltp_check_mods_desc = 'Проверить, могут ли моды работать из категорий'

}