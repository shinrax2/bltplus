local thisDir, thisFolder, upDir = BLTplusC.Dirs()


function BLTplusC:AddRequiredAction(id, clbk)
	self.required_actions = self.required_actions or {}
				
	local not_id = id
	while self.required_actions[not_id] do
		not_id = ' '..not_id
	end
	
	self.required_actions[not_id] = clbk

	self.notifications = self.notifications + 1
end


function BLTplusC:SetupMods()

	if self.updates_for_later then
		for for_mod,update_data in pairs(self.updates_for_later) do
			local found = false
			for index, cat in pairs(self.mods) do
				local mod = BLT.Mods:GetMod(cat..'/'..index)
				if mod:GetName() == for_mod then
					local found2 = false
					local update_class = update_data.repository and BLTplusRepoUpdate or BLTUpdate
					local new_update = update_class:new(mod, update_data)
					
					for k2,v2 in pairs(mod.updates) do
						if v2:GetId() == new_update:GetId() then
							mod.updates[k2] = new_update -- Prefer update+ to normal update
							found2 = true
						end
					end
					
					if not found2 then table.insert(mod.updates, new_update) end
					
					found = true
				end
			end
			if not found then
				self:AddRequiredAction(for_mod, function(item)
					self:QMenu(for_mod, string.format(BLTplusC.Loc('not_found_update'),for_mod))
					self:UpdateNotifications(-1, for_mod..'_upd_not_found')
				end)
			end
		end
		
		self.updates_for_later = nil
	end

	-- Run setup again for mods with missing dependencies
	for k,v in pairs(BLT.Mods:Mods()) do
		if next(v:GetMissingDependencies()) then
			v._errors = {}
			v:Setup()
		end
	end

	for index, cat in pairs(self.mods) do
		BLT.Mods:GetMod(cat..'/'..index):Setup()
	end
	
	
	-- Re-sort hook tables to make priorities work correctly
	local function ReSort(t)
		local files_by_mods = {}
		for k,v in ipairs(t) do
			local mod_id = v.mod:GetId()
			files_by_mods[mod_id] = files_by_mods[mod_id] or { priority = v.mod:GetPriority() }
			table.insert(files_by_mods[mod_id], v)
		end
		
		local ifiles_by_mods = {}
		for k,v in pairs(files_by_mods) do
			table.insert(ifiles_by_mods, v)
		end
		
		table.sort(ifiles_by_mods, function(a,b)
					return a.priority > b.priority
				end)
				
		local new_t = {}
		for k,v in ipairs(ifiles_by_mods) do
			for k2,v2 in ipairs(v) do
				table.insert(new_t, v2)
			end
		end
		
		return new_t
	end
	
	for k,v in pairs(BLT.hook_tables.pre) do
		BLT.hook_tables.pre[k] = ReSort(v)
	end
	for k,v in pairs(BLT.hook_tables.post) do
		BLT.hook_tables.post[k] = ReSort(v)
	end


	-- Remove uninstalled mods from checked
	for k,v in pairs(self.menu.checked_mods) do
		if not BLT.Mods:GetMod(k) then
			self.menu.checked_mods[k] = nil
		end
	end

	-- Remove uninstalled mods from update failures counter and disable updates for other mods
	for k,v in pairs(self:GetFailedUpdate()) do
		local mod = BLT.Mods:GetMod(k)
		if type(v) == 'boolean' then
			if mod and not v then 
				mod:SetUpdatesEnabled(v)
			else
				self:FailedUpdate(k, 0)
			end
		elseif not mod then
			self:FailedUpdate(k, 0)
		end
	end

	-- Remove uninstalled mods from saved repository commits
	for k,v in pairs(self.menu.repos_commits) do
		if not BLT.Mods:GetMod(k) then
			self.menu.repos_commits[k] = nil
		end
	end
end


function BLTplusC:CheckModsAndNotify()

	local mods_to_check
	for k,v in pairs(BLT.Mods:Mods()) do
		local modname = v:GetName()
		
		if v._outdated then -- A mod for older BLT
			self:AddRequiredAction(modname, function(item)
				QuickMenu:new(
						modname, 
						BLTSuperMod and BLTplusC.Loc('older_blt_loaded') or BLTplusC.Loc('older_blt_not_loaded'),
						{
							{ text = BLTplusC.Loc('yes_upgrade'),
							callback = function()
									BLTplusC.FixModTxt(v:GetPath(), v:GetJsonData())
									item:set_visible(false)
									item:set_enabled(false)
								end },
								
							{ text = BLTplusC.Loc('no'),
							is_cancel_button = true }
						}
				 ):Show()
				self:UpdateNotifications(-1,modname..'_older_blt')
			end)
		end
		
		local modfolder = v:GetPureId()
		if self.mods[modfolder] and not self.menu.checked_mods[modfolder] then
			mods_to_check = mods_to_check or {}
			mods_to_check[modfolder] = true
		end
	end

	if mods_to_check then
		local mods_str = ''
		for k,v in pairs(mods_to_check) do
			mods_str = mods_str .. k .. '\n'
		end
		
		MenuHelperHelperBLTPlus._tweaks['bltp_check_mods'] = { localized = true }
		self:AddRequiredAction('bltp_check_mods', function(item)
			QuickMenu:new(
					BLTplusC.Loc('check_compatibility'), 
					BLTplusC.Loc('mods_not_checked')..'\n'..mods_str,
					{
						{ text = BLTplusC.Loc('check_now'),
						callback = function()
								self:CheckMods(item, mods_to_check)
								
								item:set_visible(false)
								item:set_enabled(false)
								
								self:UpdateNotifications(-1)
							end },
							
						{ text = BLTplusC.Loc('check_later'),
						is_cancel_button = true }
					}
			 ):Show()
		end)
	end
end


function BLTplusC:ReadyToFindMods()

	-- It's here because this also loads up should_load settings
	self:CreateSettingsMenu()
	
	-- Add updates from updates+ for mods in 'mods'
	for k,v in pairs(BLT.Mods:Mods()) do
		if v.json_data["updates+"] then
			for k2,v2 in pairs(v.json_data["updates+"]) do
				self:AddUpdate(v, v2)
			end
		end
	end
end


function BLTplusC:FindMods()

	log("[BLT+] Loading mods")
	
	local mods_dir = BLTModManager.Constants.mods_directory

	local mods_list = {}
	local folders = file.GetDirectories(mods_dir)

	if not folders then
		return {}
	end
	
	local should_load = {}
	for k,v in pairs(folders) do
		if not BLT.Mods:GetMod(v)
			and not BLT.Mods:IsExcludedDirectory(v)
			and v ~= 'base'
			and v ~= thisFolder
		then
			self.menu.should_load[v] = true
			should_load[v] = true
		end
	end
	
	self:ReadyToFindMods()
	
	-- Remove uninstalled categories from the loaded save file data
	for k,v in pairs(self.menu.should_load) do
		if not should_load[k] then
			self.menu.should_load[k] = nil
		end
	end
		
	-- Folders in 'mods'
	for index, directory in pairs( folders ) do

		if self.menu.should_load[directory] then
			local subfolders = file.GetDirectories(BLTModManager.Constants.mods_directory .. directory .. '/')

			if not subfolders then
				return {}
			end
			
			log("[BLT+] Looking into: " .. tostring(directory))
			
			-- Folders in categories (mod folders)
			for k,v in pairs(subfolders) do
			
				local mod = BLT.Mods:GetMod(v) or self.mods[v]
				if mod then
					log("[BLT+] Mod already exists: " .. v)
					
					self:AddRequiredAction(v, function(item)
						local mod = BLT.Mods:GetMod(v)
						
						-- Loc() must be inside here, because localization is not loaded up yet
						local text = BLTplusC.Loc('two_mods')..'\n'
						text = text .. mod:GetPath() .. string.format('\nmods/%s/%s/\n\n', directory, v)
						text = text .. BLTplusC.Loc('first_loaded')
					
						self:UpdateNotifications(-1,v..'_two_mods')
						BLTplusC.QMenu(v,text)
					end)
				else
				
					local mod_path = upDir .. directory .. "/" .. v .. '/'
					local mod_defintion = mod_path .. "mod.txt"

					local file = io.open(mod_defintion)
					if file then

						log("[BLT+] Loading mod: " .. v)
					
						local file_contents = file:read("*all")
						file:close()

						local mod_content = nil
						local json_success = pcall(function()
							mod_content = json.decode(file_contents)
						end)

						if json_success and mod_content then
						
							-- Manage updates situation
							if mod_content['updates'] then
								for k2,v2 in pairs(mod_content['updates']) do
									mod_content['updates'][k2]["install_dir"] = v2["install_dir"] or upDir..directory.."/"
									mod_content['updates'][k2]["install_folder"] = v2["install_folder"] or v
								end
							end
							if mod_content['updates+'] then
								for k2,v2 in pairs(mod_content['updates+']) do
									mod_content['updates+'][k2]["install_dir"] = v2["install_dir"] or upDir..directory.."/"
									mod_content['updates+'][k2]["install_folder"] = v2["install_folder"] or v
								end
							end
						
							local new_mod = BLTMod:new( directory.."/"..v, mod_content )
							table.insert( mods_list, new_mod )
							
							new_mod:SetPureId(v)
							new_mod:SetCategory(directory)
							
							self.mods[v] = directory
						else
							log("[BLT+] An error occured while loading mod.txt from: " .. tostring(mod_path))
						end
					end
					
					-- BeardLib compatibility
					local main_xml_path = mod_path .. "main.xml"
					file = io.open(main_xml_path)
					if file then
						file:close()
						
						-- Super Post Hook?
						if not FrameworkBase then
							FrameworkBase = blt_class()
							local mt = getmetatable(FrameworkBase)
							mt = mt or {}
							local ni_orig = mt.__newindex
							mt.__newindex = function(t,k,v)
								local nv
								if k == 'FindMods'
									and not FrameworkBase.beardlib_has_its_own_code_for_bltp
								then
									nv = function(this, ...)
										v(this, ...)
										local more = FrameworkBase.hey_beardlib_look_what_i_got or nil
	
										if not more then return end
										
										local old_dir = this._directory
										
										for k2,v2 in pairs(more) do
											this._directory = v2
											v(this, ...)
										end

										this._directory = old_dir
										FrameworkBase.hey_beardlib_look_what_i_got = nil
									end
								end
								
								if ni_orig then
									ni_orig(t,k, nv or v)
								else
									rawset(t,k, nv or v)
								end
							end
							setmetatable(FrameworkBase, mt)
						end
						
						FrameworkBase.hey_beardlib_look_what_i_got = FrameworkBase.hey_beardlib_look_what_i_got or {}
						table.insert(FrameworkBase.hey_beardlib_look_what_i_got, upDir .. directory .. '/')
					end
				end
			end
		end
	end

	return mods_list
end