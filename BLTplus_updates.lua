local thisDir, thisFolder, upDir = BLTplusC.Dirs()


function BLTplusC:FailedUpdate(id, value)
	if value ~= nil then
		if not value then
			self.menu.failed_update[id] = false
		else
			self.menu.failed_update[id] = value ~= 0 and value or nil
		end
	else
		self.menu.failed_update[id] = (self.menu.failed_update[id] or 0) + 1
	end
end


function BLTplusC:GetFailedUpdate(id)
	return id == nil and self.menu.failed_update or self.menu.failed_update[id]
end


function BLTplusC:SetRepoHash(mod_id, repo_id, hash)
	self.menu.repos_commits[mod_id] = self.menu.repos_commits[mod_id] or {}
	self.menu.repos_commits[mod_id][repo_id] = hash
end


function BLTplusC:GetRepoHash(mod_id, repo_id)
	return self.menu.repos_commits[mod_id] and self.menu.repos_commits[mod_id][repo_id] or nil
end

-- ViewModGui callback
function BLTplusC:ToggleUpdates(mod)
	local id = mod:GetPureId()
	if type(self:GetFailedUpdate(id)) == 'boolean' then
		self:FailedUpdate(id, 0)
	else
		self:FailedUpdate(id, false)
	end
	self.menu.Save()
end


function BLTplusC:AddUpdate(mod, update_data, categorized)
	local update_class
	
	if update_data.repository then
		update_class = BLTplusRepoUpdate
	else
		update_class = BLTUpdate
	end
	
	if update_data.for_mod then -- Update is added for another mod
		local found = false
		for k,v in pairs(BLT.Mods:Mods()) do
			if v:GetName() == update_data.for_mod then
				local found2 = false
				local new_update = update_class:new(v, update_data)
				
				for k2,v2 in pairs(v.updates) do
					if v2:GetId() == new_update:GetId() then
						if new_update:IsPresent() then
							v.updates[k] = new_update -- Prefer update+ to normal update
						end
						found2 = true
					end
				end
				
				if not found2 and new_update:IsPresent() then
					table.insert(v.updates, new_update)
				end
				
				found = true
			end
		end
		if not found then
			if categorized then
				self:AddRequiredAction(update_data.for_mod, function(item)
					self:QMenu(mod:GetName(), string.format(BLTplusC.Loc('not_found_update'),update_data.for_mod))
					self:UpdateNotifications(-1, update_data.for_mod..'_upd_not_found_2')
				end)
			else
				-- This will be later handled in BLTplusC:SetupMods()
				self.updates_for_later = self.updates_for_later or {}
				self.updates_for_later[update_data.for_mod] = update_data
			end
		end
	else
		local new_update = update_class:new(mod, update_data)
		local found = false
		for k,v in pairs(mod.updates) do
			if v:GetId() == new_update:GetId() then
				mod.updates[k] = new_update -- Prefer update+ to normal update
				found = true
			end
		end
		if not found then table.insert(mod.updates, new_update) end
	end
end

-- Prompts to disable updates for mods that don't seem to update
function BLTplusC:CheckUpdatesError(update)

	local id = update.parent_mod:GetPureId()
	
	if update._error then
		self:FailedUpdate(id)
		
		if self:GetFailedUpdate(id) > math.random(8, 12) then
		
			self:AddNotification(id, function(item)
				self:FailedUpdate(id, 0)
				QuickMenu:new(
					id, 
					string.format(BLTplusC.Loc('mod_fails_updates'), id),
					{
						{ text = BLTplusC.Loc('yes_disable_updates'),
						callback = function()
								update.parent_mod:SetUpdatesEnabled(false)
								self:FailedUpdate(id, false)
								self.menu.Save()
							end },
							
						{ text = BLTplusC.Loc('no'), is_cancel_button = true }
					},
					true)
				self:UpdateNotifications(-1, id..'_fails_updates')
			end)
			
			self:UpdateNotifications(1)
		end
		
		if not self.save_delayed_started then
			DelayedCalls:Add('BLTPlus_got_update_save', 5, function()
				self.menu.Save()
			end)
			self.save_delayed_started = true
		end
		
	elseif self:GetFailedUpdate(id) then
		self:FailedUpdate(id, 0)
		
		if not self.save_delayed_started then
			DelayedCalls:Add('BLTPlus_got_update_save', 5, function()
				self.menu.Save()
			end)
			self.save_delayed_started = true
		end
	end
end

-- Handles notifications from updates info
function BLTplusC:CheckUpdatesInfo(update, json_data)
	if not self.menu.notifs_thru_updates then return end
	if json_data:is_nil_or_empty() then return end

	local server_data = json.decode(json_data)
	
	for _, data in pairs(server_data or {}) do
	
		if type(data.bltp_notification) == 'table' then
		
			local show = true

			if type(data.bltp_notification.expires) == 'string' then
				local expires = data.bltp_notification.expires
				if expires:match('%d%d%d%d.%d%d.%d%d %d%d:%d%d') then
					local y,mo,d,h,mi = expires:match('(%d%d%d%d).(%d%d).(%d%d) (%d%d):(%d%d)')
					local exp_time = os.time({year = tonumber(y), month = tonumber(mo),
					                            day = tonumber(d), hour = tonumber(h),
					                            min = tonumber(mi), sec = 0})
					show = exp_time > os.time()
				end
			end
			
			local id = tostring(data.bltp_notification.id)
			local last = self.menu.notifications[update.parent_mod:GetPureId()]
			if last and id and last == id then
				show = false -- Do not show the same notification twice
			end
			
			local msg = data.bltp_notification.message
			if show and id and type(msg) == 'string' then

				self.menu.notifications[update.parent_mod:GetPureId()] = id
				
				local mod_name = update.parent_mod:GetName()
				self:AddNotification(mod_name, function(item)
					self:QMenu(mod_name, msg)
					self:UpdateNotifications(-1, mod_name..'_upd_notif')
					self.menu.Save()
				end)
				
				self:UpdateNotifications(1)
			end
		end
	end
end


-- function BLTplusC:AddRepo(upd_data, mod)
	-- if upd_data.host then
		-- local modid = type(mod) == 'string' and mod or mod:GetPureId()
		-- self.repos_updates[modid] = self.repos_updates[modid] or {}
		-- table.insert( self.repos_updates[modid], upd_data.host )
	-- end
-- end