local thisDir, thisFolder, upDir = BLTplusC.Dirs()

-- Check if the game crashed in the last 5 minutes
-- Add a crashlog notification if it did
function BLTplusC:CheckCrash()
	if BLT:GetOS() ~= 'windows' then return end

	local d = os.date("!*t")
	local str = tostring(d.year)..'_'..tostring(d.month)..'_'..tostring(d.day)..'T(%d%d?)_(%d%d?)'
	
	local files = SystemFS:list('', false) or {}
	
	for i = #files, 1, -1 do
		local h, m = files[i]:match(str)
		m = m and tonumber(m) or nil
		h = h and tonumber(h) or nil
		if m and h
			and ((h==d.hour and d.min - m < 5)
				or (h+1==d.hour and 60-m+d.min < 5))
		then
			local contents
			local file = io.open('C:\\Users\\'.. os.getenv("USERNAME") ..'\\AppData\\Local\\PAYDAY 2\\crash.txt', "r")
			if file then
				contents = file:read('*all')
				file:close()
			end
			
			if contents then
				self:AddRequiredAction(BLTplusC.Loc('crashlog'), function(item)
					QuickMenu:new(
							BLTplusC.Loc('crashlog'), 
							contents,
							{
								{ text = BLTplusC.Loc('copy_crashlog'),
								callback = function()
										item:set_visible(false)
										item:set_enabled(false)
										if contents then
											local file = io.open('C:\\Documents and Settings\\'..os.getenv("USERNAME")..'\\Desktop\\crash_'..os.date('%Y.%m.%d_%H.%M')..'.txt', 'w')
											if file then
												file:write(contents)
												file:close()
											end
										end
									end },
									
								{ text = BLTplusC.Loc('ok'),
								is_cancel_button = true }
							}
					 ):Show()
					self:UpdateNotifications(-1, 'crashlog')
				end)
			end
			return
		end
	end
end

-- This checks for 'mods/' string in all of the categorized mods
-- If they link to their files by manually entered path then they won't work right
function BLTplusC:CheckMods(menuitem, only_these)
	local cr
	-- Just so that the game does not freeze
	cr = coroutine.create(function()
		if menuitem then
			menuitem:set_enabled(false)
		end
		
		local bad_mods = {}
	
		local folders = file.GetDirectories(BLTModManager.Constants.mods_directory)

		if not folders then
			return {}
		end
		
		should_check = {}
		for k,v in pairs(folders) do
			if not BLT.Mods:GetModOriginal(v)
				and not BLT.Mods:IsExcludedDirectory(v)
				and v ~= 'base'
				and v ~= thisFolder
			then
				should_check[v] = true
			end
		end
		
		local function CheckFolder(dir, fol, cat, name)
			local files = file.GetFiles(upDir..dir..'/'..fol)
					
			if not files then
				return {}
			end
			
			for k2,v2 in pairs(files) do
				local file = io.open(upDir..dir..'/'..fol..'/'..v2)
				if file then
					local file_contents = file:read("*all")
					file:close()
						
					file_contents = '\n' .. file_contents .. '\n'
					
					if file_contents:match('\n[^-]-%Amods/[^\n]+%a[^\n]+') then
						bad_mods[cat] = bad_mods[cat] or {}
						bad_mods[cat][name] = bad_mods[cat][name] or {}
						table.insert(bad_mods[cat][name], v2)
					end
				end
			end
		end
		
		for index, directory in pairs( folders ) do
			if should_check[directory] then
				local subfolders = file.GetDirectories(BLTModManager.Constants.mods_directory .. directory .. '/')

				if not subfolders then
					return {}
				end
				
				for k,v in pairs(subfolders) do
					if not only_these or only_these[v] then
						self.menu.checked_mods[v] = true
					
						CheckFolder(directory, v, directory, v)
						
						local subsubfolders = file.GetDirectories(BLTModManager.Constants.mods_directory .. directory .. '/'..v..'/')

						if not subsubfolders then
							return {}
						end
						
						for k2,v2 in pairs(subsubfolders) do
							CheckFolder(directory..'/'..v, v2, directory, v)
						end
						
						DelayedCalls:Add('BLTplusC_checking_'..directory..'_'..v, 0.05, function()
							coroutine.resume(cr)
						end)
						coroutine.yield()
					end
				end
			end
		end
		local str = ''
		for k,v in pairs(bad_mods) do
			str = str..'\n'..k..':'
			for k2,v2 in pairs(v) do
				str = str..'\n    '..k2..':'
				for k3,v3 in pairs(v2) do
					str = str..'\n        '..v3
				end
			end
			str = str..'\n'
		end
		if str == '' then
			str = BLTplusC.Loc('all_mods_work')
		else
			str = BLTplusC.Loc('these_not_work')..str
			log(str)
			str = str..'\n\n'..BLTplusC.Loc('saved_to_log')
		end
		BLTplusC.QMenu(BLTplusC.Loc('mods_check'), str)
		self.menu.Save()
	end) -- coroutine create
	coroutine.resume(cr)
end